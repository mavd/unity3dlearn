﻿using UnityEngine;
using System.Collections;

public class MoveController : MonoBehaviour {

    public float moveSpeed, rotateSpeed, rotateSpeedTower;
    public GameObject tower;

    private float move, rotate;
    private float rotateTower;

    private Rigidbody rigidbody;

    void Start () {
        rigidbody = GetComponent<Rigidbody>();
    }
	
    void FixedUpdate()
    {
        Move();
        Turn();
        TurnTower();
    }

    private void Move()
    {
        Vector3 movement = transform.forward * move * moveSpeed * Time.deltaTime;
        rigidbody.MovePosition(rigidbody.position + movement);
    }

    private void Turn()
    {
        float turn = rotate * rotateSpeed * Time.deltaTime;
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);
        rigidbody.MoveRotation(rigidbody.rotation * turnRotation);
    }

    private void TurnTower()
    {
        tower.transform.Rotate(Vector3.up, rotateTower * rotateSpeedTower * Time.deltaTime, Space.World);
    }

    void Update () {
        move = Input.GetAxis("Vertical");
        rotate = Input.GetAxis("Horizontal");

        rotateTower = Input.GetAxis("HorizontalTower");
    }
}
